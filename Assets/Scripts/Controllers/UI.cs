using System;

using TMPro;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI coinCountLable;
    [SerializeField]
    private GameObject restartPanel;

    public void Start()
    {
        GameCore.isCoinAdded += renderCoinCount;
        GameCore.isLevelEnded += showRestartPanel;
        GameCore.isLevelUnloaded += destroyAllData;

        if (restartPanel == null)
        {
            restartPanel = GameObject.Find("restartPanel");
        }
    }

    private void destroyAllData()
    {
        Destroy(gameObject);
    }

    private void showRestartPanel()
    {
        restartPanel.SetActive(true);
    }

    private void renderCoinCount(int current)
    {
        coinCountLable.text = current.ToString();
    }

    private void OnDestroy()
    {
        GameCore.isCoinAdded -= renderCoinCount;
        GameCore.isLevelEnded -= showRestartPanel;
        GameCore.isLevelUnloaded -= destroyAllData;
    }
}
