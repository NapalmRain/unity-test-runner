using System;

using UnityEngine;

public class InputController : MonoBehaviour
{
    #region "Serialized Fields" 
    [SerializeField]
    private CharacterPositionController characterPositionController;
    [SerializeField]
    private float mouseMoveDelta = 300f;
    #endregion

    
    private Vector2 mousePosition = Vector2.zero;
    private float mappingCoordCoef = 1;

    public void Start()
    {
        mappingCoordCoef = characterPositionController.CharacterMoveDelta / mouseMoveDelta;
    }

    public void SetDisabled()
    {
        characterPositionController.Stop();
        enabled = false;
    }

    public void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (mousePosition == Vector2.zero)
            {
                mousePosition = Input.mousePosition;
            }

            characterPositionController.Run();
            float currentDelta = Input.mousePosition.x - mousePosition.x;

            float xMouseDelta = currentDelta * mappingCoordCoef;
            if (Math.Abs(xMouseDelta) > 0.2f)
            {
                characterPositionController.SetNewPosition(xMouseDelta);
            }
        }
        else
        {
            characterPositionController.Stop();
            mousePosition = Vector2.zero;
        }
    }
}
