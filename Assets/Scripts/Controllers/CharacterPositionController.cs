using System;
using System.Collections;

using UnityEngine;

public class CharacterPositionController : MonoBehaviour
{
    public float CharacterMoveDelta = 2f;

    #region "Serialized Fields"
    [SerializeField]
    private Vector3 newRotation;
    [SerializeField]
    private int lerpDelta = 5;
    #endregion

    private Animator animator;
    private Vector3 newPosition;
    private float startXPosition;
    private float currentXPosition;
    private bool isRunning = false;


    public void Start()
    {
        newPosition = transform.position;
        startXPosition = newPosition.x;
        currentXPosition = startXPosition;
        animator = GetComponent<Animator>();
        StartCoroutine("ChangeIdleAnimation");
    }

    public void SetNewPosition(float xPosition)
    {

        newPosition.x = Math.Clamp(currentXPosition + xPosition, CharacterMoveDelta * -1, CharacterMoveDelta);
    }

    public void Run()
    {
        if (!isRunning)
        {
            isRunning = true;
            animator.SetBool("running", isRunning);
        }
    }

    public void Stop()
    {
        if (isRunning)
        {
            isRunning = false;
            animator.SetBool("running", isRunning);
            currentXPosition = newPosition.x;
            StartCoroutine("ChangeIdleAnimation");
        }
    }

    public void Update()
    {
        newPosition.z = transform.position.z;

        transform.rotation = Quaternion.Euler(newRotation);
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * lerpDelta);
    }

    IEnumerator ChangeIdleAnimation()
    {
        while(!isRunning)
        {
            animator.SetInteger("idle_num", UnityEngine.Random.Range(0, 3));
            yield return new WaitForSeconds(4);
        }
    }
}
