using UnityEngine;

public class CameraPositionController : MonoBehaviour
{
    [SerializeField]
    private Transform characterTransform;

    private Vector3 newPosition = new Vector3(0, 0, 0);
    private float zDelta = 0;

    public void Start()
    {
        newPosition = transform.position;
        zDelta = characterTransform.position.z - transform.position.z;
    }

    // Update is called once per frame
    public void LateUpdate()
    {
        if (characterTransform)
        {
            newPosition.z = characterTransform.position.z - zDelta;
            transform.position = newPosition;
        }
    }
}
