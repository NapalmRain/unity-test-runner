using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coin : MonoBehaviour
{
    private GameCore gameCore;

    public void Start()
    {
        gameCore = GameCore.GetInstance();
    }

    public void OnTriggerEnter(Collider other)
    {
        gameCore.AddCoin();
        Destroy(gameObject);
    }
}
