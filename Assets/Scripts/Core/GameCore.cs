using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public delegate void CoinAdded(int current);
public delegate void EmptyEvent();
public class GameCore : MonoBehaviour
{
    public static event CoinAdded isCoinAdded;
    public static event EmptyEvent isLevelEnded;
    public static event EmptyEvent isLevelUnloaded;

    #region "Serialized Fields"
    [SerializeField]
    private Transform startPosition;
    [SerializeField]
    private Transform stopPosition;
    [SerializeField]
    private GameObject coinPrefab;
    [SerializeField]
    private float coinDistance = 10;
    [SerializeField]
    private List<float> columnXCoords = new List<float>();
    #endregion

    private InputController InputController;
    private static GameCore instance;
    private int currentCoinsCount = 0;


    // Start is called before the first frame update
    public void Start()
    {
        InputController = GetComponent<InputController>();
        instance = this;

        for(int i = 1; i < Vector3.Distance(startPosition.position, stopPosition.position) / coinDistance; i++)
        {
            int index = Random.Range(0, columnXCoords.Count);

            GameObject coin = Instantiate(coinPrefab);
            coin.transform.position = new Vector3(columnXCoords[index], 1f, i * coinDistance);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        InputController.SetDisabled();
        isLevelEnded?.Invoke();
    }

    public void AddCoin()
    {
        currentCoinsCount++;
        isCoinAdded?.Invoke(currentCoinsCount);
    }

    public static GameCore GetInstance()
    {
        return instance;
    }

    public void RestartLevel()
    {
        isLevelUnloaded?.Invoke();
        SceneManager.LoadScene(0);
    }
}
